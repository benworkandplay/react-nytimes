# react-nytimes #
This is a simple study case about redux and saga. It is fetching NY Times Articles Search API. The purpose of this is to make sense of my other project - the all-react-frontend WP Theme.

## Recipe ##

- Create React App, updated to use WebPack 2

#### 'Backend' Stuff ####
- Redux
- React-Redux
- Redux-Saga

#### Presentational ####
- React
- Styled Components

#### To Do's ####
- ~~Create 'fake single page' for each articles to see how redux or whatever router deals with them.~~ After all, don't think it will make any sense since the only way to go to full content is to read it on NYTimes.
- ~~Look deeper into saga and see what to do with the other async functions that are of Thunk legacies.~~ 
- Show number of hits based on search term.
- Show paginated search results (currently it only shows first page).

## Get started ##

1. Clone the repo & cd to it
2. Yarn install
3. Yarn start
