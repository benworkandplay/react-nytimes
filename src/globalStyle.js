import { injectGlobal } from 'styled-components';
import { fontFamily } from 'utils/cssHelpers';

import 'normalize.css';

export default injectGlobal`

  * {
    box-sizing: border-box;
  }

  html,
  body {
    position: relative;
    height: 100%;
  }

  body {
    font-family: 'Montserrat', sans-serif;
    font-size: 18px;
    line-height: 1.45;
  }

  h1,
  h2,
  h3 {
    text-transform: uppercase;
    letter-spacing: 2px;
    line-height: 1.333;
  }

  h1 {
    margin: 2em 0 1em 0;
  }

  a {
    text-decoration: none;
  }

  #app {
    height: 100%;
  }

  input {
    font-family: ${fontFamily.base};
  }

  input:not([type=checkbox]):not([type=radio]):not([type=range]) {
    font-weight: 300;
    border-radius: 0;
    border-color: transparent;
    width: 100%;
    height: 50px;
    display: block;
    margin: 0;
    font-size: 18px;
  }

  input:focus {
    outline: none;
  }

  ::webkit-input-place-holder {
    font-weight: 300;
  }

  figure {
    margin: 0;
    padding: 0;
  }

`;
