import { createStore, applyMiddleware } from 'redux';
import rootReducer from 'reducers';
import createSagaMiddleware from 'redux-saga';
import sagaMonitor from 'redux-saga';
import createLogger from 'redux-logger'

import rootSaga from 'sagas';


const logger = createLogger();

const configureStore = () => {
  const sagaMiddleware = createSagaMiddleware({sagaMonitor});
  return {
    ...createStore(
      rootReducer,
      window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
      applyMiddleware(sagaMiddleware, logger)
    ), runSaga: sagaMiddleware.run(rootSaga)
  };
};

export default configureStore;
