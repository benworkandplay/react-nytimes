import { take, put, call, fork, select } from 'redux-saga/effects';
import { fetchArticlesApi } from 'api';
import * as types from 'constants/actionTypes';
import * as actions from 'actions';
import { updatedQueryTermSelector, articlesByQueryTermSelector } from 'reducers/selectors';

export function* fetchArticles(queryTerm) {
  yield put( actions.requestArticles(queryTerm) );
  const articles = yield call(fetchArticlesApi, queryTerm);
  yield put( actions.receiveArticles(queryTerm, articles));
}

export function* invalidateQueryTerm() {
  while (true) {
    const {queryTerm} = yield take(types.INVALIDATE_QUERY_TERM);
    yield call(fetchArticles, queryTerm);
  }
}

export function* nextQueryTermChange() {
  while(true) {
    const prevQueryTerm = yield select(updatedQueryTermSelector);
    yield take(types.UPDATE_QUERY_TERM);

    const newQueryTerm = yield select(updatedQueryTermSelector);
    const articlesByQueryTerm = yield select(articlesByQueryTermSelector);
    if(prevQueryTerm !== newQueryTerm && !articlesByQueryTerm[newQueryTerm]) {
      yield fork(fetchArticles, newQueryTerm);
    }
  }
}

export function* startup() {
  const updatedQueryTerm = yield select(updatedQueryTermSelector);
  yield fork(fetchArticles, updatedQueryTerm);
}

export default function* root() {
  yield fork(startup);
  yield fork(nextQueryTermChange);
  yield fork(invalidateQueryTerm);
}
