const media = ({multimedia}) => {
  multimedia.map(({height, legacy, subtype, url, width }) => ({
    height: height,
    width: width,
    url: url,
    legacy: legacy
  }));
}
