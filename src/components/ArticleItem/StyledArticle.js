import styled from 'styled-components';
import { colorTheme } from 'containers/ColorTheme';
import palette from 'utils/palette';

export default colorTheme(styled.article`
  margin: auto;
  padding: 18px 0 36px;
  border-bottom: 10px solid ${palette.clay};

  .entry-thumbnail {
    margin: 0;
  }

  .entry-thumbnail img {
    width: 100%;
    height: auto;
  }

  .entry-header {
    margin: 18px 0 36px;
  }

  .entry-headline {
    text-align: center;
    letter-spacing: 2px;
    color: ${palette.lynch.darken(0.25)};
  }

  .entry-headline a {
    text-decoration: none;
    color: ${palette.lynch.darken(0.25)};
  }

  .entry-headline a:hover,
  .entry-headline a:active {
    color: ${palette.lynch.darken(0.5)};
  }

  .entry-meta span {
    text-align: center;
    display: block;
    font-size: 14px;
    color: ${palette.lynch.lighten(0.25)};
  }

  .entry-snippet {
    text-align: center;
  }

  .entry-snippet p {
    color: ${palette.lynch};
    text-align: justify;
  }
`);
