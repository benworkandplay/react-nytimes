import styled from 'styled-components';

const ListItem =  styled.li`
  margin: 36px 0;

  &:first-child {
    margin-top: 0;
  }
`;

export default ListItem;
