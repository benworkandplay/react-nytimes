import React from 'react';
// import { Link } from 'react-router';
import moment from 'moment';
import ListItem from './StyledLi';
import Article from './StyledArticle';

export default ( article, onClick, handleSelectArticle ) => (
  <ListItem className="article-wrapper">
    <Article className="article">
      {article.multimedia.length ?
        <figure className="entry-thumbnail">
          <img src={`http://thenewyorktimes.com/${article.multimedia[1].url}`} alt={article.headline.main} />
        </figure> : null }
        <header className="entry-header">
          <h2 className="entry-headline" onClick={(handleSelectArticle)=>{}}>
            {article.headline.main}
          </h2>
          <div className="entry-meta">
            { article.byline !== null &&
            <span className="source">{article.byline.original}</span> }
            <span className="date">{moment(article.pub_date).fromNow()}</span>
          </div>
        </header>
        <section className="entry-snippet">
          <p>{article.lead_paragraph}</p>
        </section>
    </Article>
  </ListItem>
);
