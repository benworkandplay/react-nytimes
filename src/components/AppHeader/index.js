import React from 'react';
import AppHeader from './StyledHeader';
import Logo from 'components/Logo';

export default () =>
  <AppHeader className="app-header">
    <Logo className="app-logo" />
  </AppHeader>
