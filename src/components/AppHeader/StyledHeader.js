import styled from 'styled-components';

export default styled.header`
  position: relative;
  background-color: #222;
  padding: 0 36px;
  width: 100%;
  height: 68px;
  position: fixed;
  top: 0;
  left: 0;

  @media (max-width: 768px) {
    padding: 4px 36px;
  }
`;
