import styled from 'styled-components';

export default styled.h1`
  margin: 2em 0 0.5em;
`;
