import React from 'react';
import SearchWrapper from './StyledSearchBar';
import InputSearch from './InputSearch';
import { Debounce } from 'react-throttle';
import Section from 'components/Section';

export default ({onChange, onSubmit, searchQuery}) =>
  <Section className="search-bar">
    <Debounce time="1000" handler="onChange">
      <SearchWrapper onChange={onChange} onSubmit={onSubmit}>
        <InputSearch placeholder="Type anything to start" type="search" value={searchQuery} />
      </SearchWrapper>
    </Debounce>
  </Section>
