import styled from 'styled-components';
import { clearFix } from 'utils/cssHelpers';
import { colorTheme } from 'containers/ColorTheme';
import palette from 'utils/palette';

export default colorTheme(styled.form`
  ${ clearFix };
  border-bottom: 1px solid ${palette.clay.darken(0.2)};
  margin: 0 0 36px 0;
`);
