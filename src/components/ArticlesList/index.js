import React from 'react';
import Article from 'components/ArticleItem';
import Section from 'components/Section';
import ListWrapper from './StyledList';
import Spinner from 'components/Spinner';

export default ({articles, isFetching}) => (
  <Section className="search-results">
    { isFetching ? <Spinner /> :
    <ListWrapper className="artilcles">
      { articles.map( (article) => {
        return (
          <Article
            key={article._id}
            {...article}
          />
        );
      })}
    </ListWrapper> }
  </Section>
);
