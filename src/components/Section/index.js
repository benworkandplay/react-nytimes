import React, { PropTypes} from 'react';
import Wrapper from './StyledSection';

const Section = (props) =>
  <Wrapper {...props}>
    <div className="container">
      {props.children}
    </div>
  </Wrapper>

Section.propTypes = {
  children: PropTypes.node
}

export default Section;
