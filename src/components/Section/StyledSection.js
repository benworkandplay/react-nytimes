import styled from 'styled-components';
import { clearFix } from 'utils/cssHelpers'

export default styled.section`
  text-align: center;
  ${clearFix}
  .container {
    overflow: auto;
    width: 100%;
    max-width: 900px;
    padding: 0 36px;
    margin: auto;
  }

  &:last-child {
    padding-bottom: 72px;
  }

  @media (min-width: 1280px ) {
    .container {
      width: 50%;
    }
  }

  @media (min-width: 1024px) {
    .container {
      width: 60%;
    }
  }

  @media (min-width: 768px) and (max-width: 1279px) {
    .container {
      width: 75%;
    }
  }

`;
