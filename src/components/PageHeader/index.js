import React, { PropTypes } from 'react';
import Section from 'components/Section';
import H1 from 'components/H1';

const PageHeader = ({title, children}) =>
  <Section className="page-header">
    <H1 className="page-title">{title}</H1>
    {children}
  </Section>

PageHeader.propTypes = {
  title: PropTypes.string,
  children: PropTypes.node,
}

export default PageHeader;
