import styled, { keyframes } from 'styled-components';
import { colorTheme } from 'containers/ColorTheme';
import palette from 'utils/palette';

const skBounceDelay = keyframes`
  0%,
  80%,
  100% {
    transform: scale(0);
  }

  40% {
    transform: scale(1.0);
  }
`;

export default colorTheme(styled.div`
  position: fixed;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  width: 79px;
  text-align: center;

  .spinner-bounce {
    width: 18px;
    height: 18px;
    background-color: ${palette.clay}
    border-radius: 100%;
    display: inline-block;
    margin: 3px;
    animation: ${skBounceDelay} 1.4s infinite ease-in-out both;
  }

  .spinner-bounce.bounce-one {
    -webkit-animation-delay: -0.32s;
    animation-delay: -0.32s;
  }

  .spinner-bounce.bounce-two {
    -webkit-animation-delay: -0.16s;
    animation-delay: -0.16s;
  }
`);
