import React from 'react'
import Spinner from './Spinner'

export default () =>
  <Spinner className="spinner">
    <div className="spinner-bounce bounce-one"></div>
    <div className="spinner-bounce bounce-two"></div>
    <div className="spinner-bounce bounce-three"></div>
  </Spinner>;
