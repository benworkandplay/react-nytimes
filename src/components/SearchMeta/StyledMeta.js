import styled from 'styled-components';
import { colorTheme } from 'containers/ColorTheme';
import palette from 'utils/palette';

export default colorTheme(styled.p`
  margin: 0;
  font-size: 14px;
  color: ${palette.lynch.lighten(0.5)}
  text-transform: uppercase;
`);
