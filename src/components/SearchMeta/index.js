import React from 'react';
import Section from 'components/Section';
import P from './StyledMeta';

export default ({lastUpdated}) => (
  <Section className="search-meta">
    {lastUpdated &&
    <P className="last-updated">Last updated at {new Date(lastUpdated).toLocaleTimeString('en-US')}</P>
    }
  </Section>
);
