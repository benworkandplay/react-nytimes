import styled from 'styled-components';
import { fontFamily } from 'utils/cssHelpers';

export default styled.input`
  font-family: ${fontFamily.base};
  text-transform: uppercase;
  letter-spacing: 1px;
  font-size: 18px!important;
  display: block;
  width: 100%;
  height: 60px;
  padding: 0;
  text-align: center;
`;
