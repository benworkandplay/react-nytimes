import React from 'react';
import Logo from './LogoWrapper';
import { IndexLink } from 'react-router';

export default () =>
  <IndexLink to="/" className="app-logo">
    <Logo>
      <span className="sr-text">The New York Times Search</span>
    </Logo>  
  </IndexLink>
