import styled from 'styled-components';
import NYTimesLogo from './nytimes.svg';
import { screenReaderText } from 'utils/cssHelpers';

export default styled.span`
  display: block;
  margin: 18px auto;
  width: 229px;
  height: 32px;

  background: url(${NYTimesLogo}) center no-repeat;

  span.sr-text {
    ${screenReaderText}
  }

  @media (max-width: 768px) {
    width: 221px;
    height: 24px;
  }

`;
