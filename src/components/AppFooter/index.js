import React from 'react';
import Footer from './StyledFooter';

export default () =>
  <Footer className="app-footer">
    <div className="footer-inner">
      <p>&copy;2017 <a href="http://www.benworkandplay.com" target="_blank">Ben, WorkandPlay</a></p>
    </div>
  </Footer>
