import styled from 'styled-components';
import { colorTheme } from 'containers/ColorTheme';
import palette from 'utils/palette';

export default colorTheme(styled.footer`
  padding: 36px;
  text-align: center;
  position: static;
  background-color: ${palette.ebony};

  .footer-inner {
    display: table;
    position: relative;
  }

  p.footer-text {
    color: ${palette.clouds};
    margin: 0;
  }
`);
