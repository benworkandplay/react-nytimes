import styled from 'styled-components';

export default styled.main`
  height: 100%;

  &::before {
    content: '';
    width: 100%;
    height: 68px;
    display: block;
  }

  .main-content {
    max-width: 900px;
    min-height: 100%;
  }

  @media (min-width: 1280px) {
    .main-content {
      width: 50%;
      padding: 0;
    }
  }

  @media (max-width: 768px) {
    .main-content {
      width: 100%;
      padding: 0 36px;
    }
  }

`;
