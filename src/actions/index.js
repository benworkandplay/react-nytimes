import * as types from 'constants/actionTypes';

export const updateQueryTerm = (queryTerm) => ({
  type: types.UPDATE_QUERY_TERM,
  queryTerm
});

export const emptyQueryTerm = (queryTerm) => ({
  type: types.EMPTY_QUERY_TERM,
  queryTerm
})

export const invalidateQueryTerm = (queryTerm) => ({
  type: types.INVALIDATE_QUERY_TERM,
  queryTerm
});

export const requestArticles = (queryTerm) => ({
  type: types.REQUEST_ARTICLES,
  queryTerm
});

export const receiveArticles = (queryTerm, articles) => ({
  type: types.RECEIVE_ARTICLES,
  queryTerm,
  articles,
  receivedAt: Date.now()
});
