import { combineReducers } from 'redux';
import * as types from 'constants/actionTypes';

const updatedQueryTerm = (state = 'stories', action) => {
  switch (action.type) {
    case types.UPDATE_QUERY_TERM:
      return action.queryTerm
    case types.EMPTY_QUERY_TERM:
      return {
        ...state
      }
    default:
      return state
  }
};

const articles = (state = {
  isFetching: false,
  items: []
}, action) => {
  switch (action.type) {
    case types.REQUEST_ARTICLES:
      return {
        ...state,
        isFetching: true,
      }

    case types.RECEIVE_ARTICLES:
      return {
        ...state,
        isFetching: false,
        items: action.articles.docs,
        lastUpdated: action.receivedAt,
      }
    default:
      return state
  }
}

const articlesByQueryTerm = (state = {}, action) => {
  switch (action.type) {
    case types.REQUEST_ARTICLES:
    case types.RECEIVE_ARTICLES:
      return {
        ...state,
        [action.queryTerm]: articles(state[action.queryTerm], action)
      }
    default:
      return state
  }
}

const rootReducer = combineReducers({
  articlesByQueryTerm,
  updatedQueryTerm,
});

export default rootReducer;
