import React, { Component } from 'react';
import { ThemeProvider } from 'styled-components';
import { colors } from 'utils/cssHelpers';

export const colorTheme = (ComposedComponent) =>
  class ColorTheme extends Component {
    render() {
      return (
        <ThemeProvider theme={colors}>
          <ComposedComponent {...this.props} {...this.state} />
        </ThemeProvider>
      );
    }
  };
