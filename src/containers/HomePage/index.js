import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { updateQueryTerm } from 'actions';

import PageWrapper from 'components/PageWrapper';
import PageHeader from 'components/PageHeader';
import SearchBar from 'components/SearchBar';
import SearchMeta from 'components/SearchMeta';
import Section from 'components/Section';
import ArticleList from 'components/ArticlesList';
import Spinner from 'components/Spinner';

class HomePage extends Component {
  static propTypes = {
    updatedQueryTerm: PropTypes.string.isRequired,
    articles: PropTypes.array.isRequired,
    isFetching: PropTypes.bool.isRequired,
    lastUpdated: PropTypes.number,
    dispatch: PropTypes.func.isRequired
  }

  handleOnChange = (event) => {
    const inputQuery = (event.target.value).toLowerCase();
    this.nextQueryTerm = inputQuery;
    this.handleOnSubmit(event);
  }

  handleOnSubmit = (event) => {
    event.preventDefault();
    if ( this.nextQueryTerm === '' ) {
      return;
    }
    this.props.dispatch(updateQueryTerm(this.nextQueryTerm));
  }

  render() {
    const { updatedQueryTerm, articles, isFetching, lastUpdated } = this.props;
    return (
      <PageWrapper className="page-wrapper">
        <PageHeader
          title="Search the Times"
        />
        <SearchBar
          value={updatedQueryTerm}
          className="search-bar"
          onChange={this.handleOnChange}
          onSubmit={this.handleOnSubmit}
        />
      { lastUpdated && <SearchMeta {...this.props} /> }
      { isFetching ? <Spinner /> : ( articles.length ? <ArticleList {...this.props} /> : <Section className="search-results"><h2>No Articles Found</h2></Section> ) }
      </PageWrapper>
    );
  }
}

const mapStateToProps = (state) => {
  const {updatedQueryTerm, articlesByQueryTerm} = state;
  const {
    isFetching,
    lastUpdated,
    items: articles,
  } = articlesByQueryTerm[updatedQueryTerm] || {
    isFetching: true,
    items: []
  }

  return {
    updatedQueryTerm,
    articles,
    isFetching,
    lastUpdated
  }
}

export default connect (mapStateToProps)(HomePage);
