import styled from 'styled-components';

export default styled.section`
  overflow: auto;

  .section-header {
    text-align: center;
    text-transform: uppercase;
  }

  .section-header h1 {
    margin: 0;
  }
`;
