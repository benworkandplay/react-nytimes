import React from 'react';
import AppWrapper from './AppWrapper';
import AppHeader from 'components/AppHeader';

export default (props) => (
  <AppWrapper className="app-wrapper">
    <AppHeader className="app-header" />
    {props.children}
  </AppWrapper>
);
