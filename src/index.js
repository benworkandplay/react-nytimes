import React from 'react';
import { Router, browserHistory } from 'react-router';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from './store';
import routes from './routes';

import FontFaceObserver from 'fontfaceobserver';
const montSerratObserver = new FontFaceObserver('Montserrat', {});
montSerratObserver.load().then(() => {
  document.body.classList.add('fontLoaded');
}, () => {
  document.body.classList.remove('fontLoaded');
});

import './globalStyle';

const store = configureStore();
store.subscribe(() => {
  console.log(store.getState());
});

render(
  <Provider store={store}>
    <Router history={browserHistory} routes={routes} />
  </Provider>, document.getElementById('app')
);
