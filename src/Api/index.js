import fetch from 'isomorphic-fetch';
const API_KEY = '20c765b692134758a020e4769e03a490';

export const fetchArticlesApi = (queryTerm) => {
  const NEWYORKTIMES_API_ENDPOINT = `https://api.nytimes.com/svc/search/v2/articlesearch.json?api-key=${API_KEY}&sort=newest&q=${queryTerm}`;

  return fetch(NEWYORKTIMES_API_ENDPOINT)
    .then(response => {
      return response.json().then(articles => {
        return articles.response
      })
    })
}
