export const clearFix = {
  "&::after": {
    content: '',
    display: 'table',
    clear: 'both',
  }
}

export const colors = {
  clay: '#FDE3A7',
  ebony: '#22313F',
  lynch: '#6C7A89',
  clouds: '#ecf0f1'
}

export const fontFamily = {
  base: '"Montserrat", sans-serif'
}

export const screenReaderText = {
  clip: 'rect(1px, 1px, 1px, 1px)',
  height: '1px',
  width: '1px',
  overflow: 'hidden',
  position: 'absolute'
}
