import createTheme from 'styled-components-theme';
import { colors } from './cssHelpers';

export default createTheme(...Object.keys(colors));
